import {Quote} from "./quote"

export class Movie {
    public show: boolean = true;

    constructor(public name: string, public imageLocation: string, public quotes: Quote[]) {
    }

    randomQuote() {
        return this.quotes[Math.floor(Math.random() * this.quotes.length)];
    }
}