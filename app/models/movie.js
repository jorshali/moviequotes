"use strict";
var Movie = (function () {
    function Movie(name, imageLocation, quotes) {
        this.name = name;
        this.imageLocation = imageLocation;
        this.quotes = quotes;
    }
    return Movie;
}());
exports.Movie = Movie;
