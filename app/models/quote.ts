export class Quote {
    constructor(public character: string, public text: string, public imageLocation: string) {
    }
}