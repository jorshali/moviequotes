import {Page} from 'ionic-angular';
import {Movies} from "../movies/movies";
import {Config} from '../config/config';

@Page({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = Movies;
  tab2Root: any = Config;
}
