import {Page} from 'ionic-angular';
import {MovieService} from "../../services/movieService";
import {Movie} from "../../models/movie";

@Page({
  templateUrl: 'build/pages/config/config.html',
})
export class Config {
  public movies: Movie[];

  constructor(public movieService: MovieService) {
    this.movies = movieService.getMovies();
  }
}
