import {Modal, NavParams, NavController, Page, ViewController} from 'ionic-angular';

@Page({
    template: `
  <ion-content padding>
    <h2>{{quote.character}}</h2>
    <p>{{quote.text}}</p>
    <ion-avatar>
        <img src="{{quote.imageLocation}}">
    </ion-avatar>
    <button (click)="close()">Close</button>
  </ion-content>`
})
export class QuoteModal {
    public quote: string;

    constructor(private viewCtrl: ViewController, private params: NavParams) {
        this.quote = params.get('quote');
    }

    close() {
        this.viewCtrl.dismiss();
    }
}