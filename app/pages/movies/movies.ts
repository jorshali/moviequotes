import {Page} from 'ionic-angular';
import {MovieService} from "../../services/movieService";
import {Movie} from "../../models/movie";
import {QuoteModal} from "./quoteModal";
import {Modal, NavController} from 'ionic-angular'

@Page({
  templateUrl: 'build/pages/movies/movies.html',
})
export class Movies {
  public movies: Movie[];

  constructor(private movieService: MovieService, private nav: NavController) {
  }
  
  movieSelected(movie: Movie) {
    let modal = Modal.create(QuoteModal, { quote: movie.randomQuote() });
    this.nav.present(modal);
  }
  
  onPageWillEnter() {
    this.movies = this.movieService.getMovies();
  }
}
