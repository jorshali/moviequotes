import {Injectable} from "@angular/core";
import {Movie} from "../models/movie";
import {Quote} from "../models/quote";

@Injectable()
export class MovieService {
    private _movies: Movie[] = [
        new Movie("Waterboy", "", [
            new Quote("Bobby Boucher", "Now that's some high quality H2O", "images/bobby.jpg"),
            new Quote("Bobby Boucher", "Tackling fuel", "images/bobby.jpg"),
            new Quote("Bobby Boucher", "You're drinking the wrong water!", "images/bobby.jpg"),
            new Quote("Momma", "You're not playing any of that foosball", "images/momma.jpg"),
            new Quote("Bobby Boucher", "Captain Insano shows no mercy", "images/bobby.jpg")
        ]),
        new Movie("Nacho Libre", "", [
            new Quote("Nacho", "Get that corn outta my face!", "images/nacho.jpg"),
            new Quote("Massage guy", "Ramsay is number 1, his eyes are number 1, his arms are number 1...", "images/ramsay.jpg"),
            new Quote("Nacho", "Hey, let go my blouse", "images/nacho.jpg"),
            new Quote("Nacho", "They think I do not know a buttload of crap about the Gospel, but I do!", "images/nacho.jpg"),
            new Quote("Nacho", "When you're a man, sometimes you wear stretchy pants in your room.  It's for fun.", "images/nacho.jpg")
        ]),
        new Movie("Napolean Dynamite", "", [
            new Quote("Napolean", "Nunchuck skills… bowhunting skills… computer hacking skills… Girls only want boyfriends who have great skills!", "images/napoleon.jpg"),
            new Quote("Rex", "You think anybody wants a roundhouse kick to the face while I’m wearing these bad boys? Forget about it.", "images/instructor.jpg"),
            new Quote("Uncle Rico", "How much you wanna make a bet I can throw a football over them mountains?", "images/uncle_rico.jpg"),
            new Quote("Pedro", "Vote for me, and all your wildest dreams will come true.", "images/pedro.jpg")
        ])
    ];

    constructor() {
    }

    getMovies() {
        return this._movies.filter((movie : Movie) => { return movie.show });
    }
}